# Samply caDSR Client

This client offers convenient methods to access the caDSR REST interface.

When using this client always keep in mind, that the caDSR is *very* slow
(~20 secods per request).

# IMPORTANT

As of September 1st, 2017, the repository has moved to https://bitbucket.org/medicalinformatics/samply.mdr.adapter.cadsr. Please see the following help article on how to change the repository location in your working copies:

    https://help.github.com/articles/changing-a-remote-s-url/

If you have forked Samply.MDR in Bitbucket, the fork is now linked to the new location automatically. Still, you should change the location in your local copies (usually, the origin of the fork is configured as a remote with name "upstream" when cloning from Bitbucket).